Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        size++;
    }
    return size;
};

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) 
  	return parts.pop().split(";").shift();

  return 1;
}


$(function(){

	var idtv = {
		contents: [],
		settings: null,
		page: getCookie('idtv-birthdate-pagination'),
		totalPages : 0,
		init: function(contents, settings){

			var self = this;
			var time = settings.display_time - 11;
			var interval = time / settings.transitionsPerCall;
			
			var i = 1;
			var generalIndex = 1;
			self.contents[i] = [];

			$.each(contents, function(index, content){
				self.contents[i].push(content);
				if( (generalIndex % 6) == 0 ){
					i++;
					self.contents[i] = [];
				}

				self.totalPages = i;
				generalIndex++;
			});

			self.settings = settings;

			var intro = document.getElementById("intro");
			var loop = document.getElementById("loop");

			intro.oncanplay  = function(){
				this.play();
			}
			intro.onended = function(){

				$('#intro').css('display', 'none');
				loop.play();
				self.show();
				
				var max = 1;
				var intervalInstance = setInterval(function(){
					if( max == settings.transitionsPerCall ){
						clearInterval(intervalInstance);
						console.log( 'Max pages achived: ' + max );
					}else{
						self.show();
						max++;
					}
				}, interval * 1000);				
			}			
		},
		show: function(){

			var self = this;
			console.log(this.contents);

			$('.employes').fadeOut('slow', function(){
				$(this).html('');

				$.each( self.contents[self.page], function(index, content){

					var article = document.createElement('article');
					var time = document.createElement('time');
					var name = document.createElement('h2');
					var role = document.createElement('h3');

					var xmlDate = content.date.split('/');
					var todaysDate = new Date();
					var birthDate = new Date( todaysDate.getFullYear(), parseInt(xmlDate[1]) - 1, xmlDate[0] );
					
					birthDate.setHours(0,0,0,0);
					todaysDate.setHours(0,0,0,0);

					if(birthDate.toDateString() == todaysDate.toDateString()) {
					    $(article).addClass('today');
					}

					$(time).text( xmlDate[0] + '/' + xmlDate[1] ).appendTo(article);
					$(name).text( content.firstname + ' ' + content.lastname ).appendTo(article);
					$(role).text( content.position ).appendTo(article);

					$('.employes').append(article);

					if( (index + 1) == self.contents[self.page].length ){

						$('.employes').fadeIn('slow');

						if( self.page == self.totalPages ){
							self.page = 1;
						}else{
							self.page++;
						}
						document.cookie = "idtv-birthdate-pagination=" + self.page;
					}
				});
			});	

			
		}
	};
	
	$.ajax({
		url: 'configuration.json',
		dataType: 'text'
	}).done(function(response) {
		
		var json = $.parseJSON(response);
		var settings = json.idtv;
		
		$.ajax({
		  url: settings.content_filename,
		  dataType: 'xml',
		  success: function(dataResponse){

		  	var contents = {};
		  	var $xml = $(dataResponse);
		  	var $contents = $xml.find("special_contents special_values");

		  	 $.each( $contents, function(index, content){

		  	 	var $content = $(content);
		  	 	var id = $content.attr('id');

		  	 	var $FirstName = $content.find('FirstName');
		  	 	var $LastName = $content.find('LastName');
		  	 	var $Position = $content.find('Position');
		  	 	var $Date = $content.find('Date');

		  	 	contents[id] = { 
		  	 		firstname: $FirstName.text(),
		  	 		lastname: $LastName.text(),
		  	 		position: $Position.text(),
		  	 		date: $Date.text()
		  	 	 };
		  	});

		  	idtv.init( contents, settings );

		 },
		  error: function(jqXHR, textStatus, errorThrown){
		  	console.log("error " + textStatus);
        	console.log("incoming Text " + jqXHR.responseText);
		  }
		});

	}).fail(function(jqXHR, textStatus, errorThrown) {
	    console.log("error " + textStatus);
        console.log("incoming Text " + jqXHR.responseText);
	});

});